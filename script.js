document.addEventListener("keydown", function (evt) {
	const btn = document.querySelectorAll('.btn');
	btn.forEach((item) => {
		item.style.background = '';
		const btnAtrr = item.getAttribute("data-id")
		if (evt.code === btnAtrr) {
			item.style.background = 'blue';
		}
	})
})
